package com.sepp.flowplus.financial;

import com.google.common.collect.Maps;
import org.flowable.engine.*;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.impl.cfg.StandaloneProcessEngineConfiguration;
import org.flowable.engine.repository.Deployment;
import org.flowable.task.api.Task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FinancialReport {


    public static void main(String[] args) {
        ProcessEngineConfiguration cfg = new StandaloneProcessEngineConfiguration()
                .setJdbcUrl("jdbc:h2:mem:flowable;DB_CLOSE_DELAY=-1")
                .setJdbcUsername("sa")
                .setJdbcPassword("")
                .setJdbcDriver("org.h2.Driver")
                .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);

        //通过配置获取engine
        ProcessEngine processEngine = cfg.buildProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //部署对应的流程（xml、bpmn）
        Deployment deployment = repositoryService.createDeployment()
                .addClasspathResource("FinancialReportProcess.bpmn20.xml")
                .deploy();
        RuntimeService runtimeService = processEngine.getRuntimeService();
        Map<String,Object> variables = new HashMap<>();
        variables.put("dateVariable", "PT1M");
        variables.put("group","chenlang");
        variables.put("owner","asd");
//        variables.put("endAuditor","zhangsan");
        // Start a process instance
        String procId = runtimeService.startProcessInstanceByKey("financialReport",variables).getId();

        // Get the first task
        TaskService taskService = processEngine.getTaskService();
        List<Task> tasks = taskService.createTaskQuery().taskAssignee("chenlang").list();
        for (Task task : tasks) {
            System.out.println(task.getAssignee());
            HashMap<String, Object> map = Maps.newHashMap();
            map.put("endAuditor","zhangsan1");
            runtimeService.setVariables(task.getExecutionId(),map);
            taskService.complete(task.getId());

        }


        System.out.println("Number of tasks for fozzie: "
                + taskService.createTaskQuery().taskAssignee("fozzie").count());

        // Retrieve and claim the second task
        tasks = taskService.createTaskQuery().taskCandidateGroup("management").list();
        tasks = taskService.createTaskQuery().taskAssignee("zhangsan1").list();
        for (Task task : tasks) {
            System.out.println("Following task is available for management group: " + task.getName());
//            taskService.claim(task.getId(), "kermit");
            taskService.complete(task.getId());

        }
        tasks = taskService.createTaskQuery().taskAssignee("kermit").list();
        // Completing the second task ends the process
        for (Task task : tasks) {
            taskService.complete(task.getId());
        }

        // verify that the process is actually finished
        HistoryService historyService = processEngine.getHistoryService();
        HistoricProcessInstance historicProcessInstance =
                historyService.createHistoricProcessInstanceQuery().processInstanceId(procId).singleResult();
        System.out.println("Process instance end time: " + historicProcessInstance.getEndTime());

    }
}
