package com.sepp.flowplus.jdbc;

import com.sepp.flowplus.FlowplusApplication;
import com.sepp.flowplus.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

//@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = FlowplusApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class JdbcTemplateTest {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Test
    public void test01() {
        String sql = "select user_name userName,user_account userAccount,user_email userEmail from sepp_user u where user_account = ?";
        RowMapper<User> rowMapper = new BeanPropertyRowMapper<User>(User.class);
        User count = jdbcTemplate.queryForObject(sql, rowMapper,"LIUYI");
        System.out.println(count);
    }


}
