package com.sepp.flowplus.model.constants;

/**
 * 审批状态
 */
public enum AuditStatus {
    APPROVE("已同意"),
    REJECT("被驳回"),
    RETURN("被踢退"),
    EXPIRED("已过期")
    ;

    private String name;

    AuditStatus(String name) {
        this.name = name;
    }

    public static String processInstanceResult(String endActId) {
        AuditStatus auditStatus = valueOf(AuditStatus.class, endActId.toUpperCase());
        return auditStatus.name;
    }


}
