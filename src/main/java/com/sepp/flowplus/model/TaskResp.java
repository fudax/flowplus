package com.sepp.flowplus.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.task.api.Task;
import org.flowable.task.api.history.HistoricTaskInstance;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskResp {

    private String id;
    private String processInstanceId;
    private String processDefinitionName;
    private String taskId;
    private String assignee;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    private String owner;
    private String name;
    private Date dueDate;
    private String starter;

    private String processDefinitionKey;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    private String businessKey;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    private Integer processDefinitionVersion;
    private String description;
    private String deploymentId;
    private long durationInMillis;
    private String category;
    private String taskDefinitionKey;
    private String tenantId;
    private String currentUser;
    private String currentNode;
    private List<String> users;

    //任务剩余时间过期
    private long time;



    private Map<String, Object> processVariables;


    public static TaskResp apply(Task task) {
        return TaskResp.builder().createTime(task.getCreateTime())
                .dueDate(task.getDueDate())
                .assignee(task.getAssignee())
                .owner(task.getOwner())
                .name(task.getName())
                .taskId(task.getId()).build();
    }

    public static TaskResp apply(HistoricTaskInstance historicTaskInstance) {
        return TaskResp.builder()
                .assignee(historicTaskInstance.getAssignee())
                .dueDate(historicTaskInstance.getDueDate())
                .processDefinitionKey(historicTaskInstance.getTaskDefinitionKey())
                .name(historicTaskInstance.getName())
                .taskId(historicTaskInstance.getId())
                .assignee(historicTaskInstance.getAssignee())
                .owner(historicTaskInstance.getOwner())
                .endTime(historicTaskInstance.getEndTime())
                .startTime(historicTaskInstance.getCreateTime())
                .build();
    }


    public static TaskResp apply(HistoricProcessInstance instance) {

        return TaskResp.builder()
                .starter(instance.getStartUserId())
                .processInstanceId(instance.getId())
                .processDefinitionName(instance.getProcessDefinitionName())
                .name(instance.getProcessDefinitionName())
                .processDefinitionKey(instance.getProcessDefinitionKey())
                .processDefinitionVersion(instance.getProcessDefinitionVersion())
                .startTime(instance.getStartTime())
                .endTime(instance.getEndTime())
                .build();
    }


}
