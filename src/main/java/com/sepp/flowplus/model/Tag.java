package com.sepp.flowplus.model;

import lombok.Data;

@Data
public class Tag {

    private String tagKey;
    private Object tagValue;

    public static Tag buildTag(String key, Object value) {
        Tag tag = new Tag();
        tag.setTagKey(key);
        tag.setTagValue(value);
        return tag;
    }
}
