package com.sepp.flowplus.model;

import com.sepp.flowplus.model.constants.AuditStatus;
import lombok.Data;

import java.util.List;
import java.util.Map;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toMap;

@Data
public class TaskVariable {
    private List<Tag> tags;
    private String processDefinitionKey;
    private String processId;
    private String starter;
    private String starterName;
    private String taskId;
    private AuditStatus auditStatus;
    private String delegateUser;

    public static Map<String, Object> tagsToMap(List<Tag> tags) {
        return tags.stream()
                .filter(tag -> nonNull(tag.getTagKey()) && nonNull(tag.getTagValue()))
                .collect(toMap(Tag::getTagKey, Tag::getTagValue));
    }
}
