package com.sepp.flowplus.model.history;

import com.sepp.flowplus.model.constants.AuditStatus;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class ActHiNodeStatus {

    private Long id;
    private String processId;
    private String taskId;
    private String assignee;
    private AuditStatus status;
    private Timestamp createdAt;
    private Timestamp updatedAt;

}
