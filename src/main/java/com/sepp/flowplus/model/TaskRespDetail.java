package com.sepp.flowplus.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskRespDetail {
    private List<TaskResp> rejectedTasks;
    private List<TaskResp> pendingTasks;
    private List<TaskResp> expiredTasks;
    private List<TaskResp> approvedTasks;
    private List<TaskResp> returnTasks;



}
