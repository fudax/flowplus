package com.sepp.flowplus.model;

import lombok.Data;

import java.util.Date;

@Data
public class TaskQueryCondition {

    private Date start;
    private Date end;
    private String userId;
    private Integer pageNum;
    private Integer pageSize;


    public Integer firstNum() {
        return (pageNum - 1) * pageSize;
    }

}
