package com.sepp.flowplus.model;

import lombok.Data;

import java.sql.Date;

/**
 * created by lchen on 2020/2/18
 */
@Data
public class ProcessInstance {

    private Long id;
    private String processInstanceId;
    private String name;
    private String user;
    private String version;
    private Date createTime;
    private Date updateTime;

}
