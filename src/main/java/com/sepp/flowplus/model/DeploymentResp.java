package com.sepp.flowplus.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.flowable.engine.repository.ProcessDefinition;

import java.util.Date;


/**
 * created by lchen on 2020/2/18
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeploymentResp {

    private String id;
    private String processId;
    private String processKey;
    private String name;
    private String resourceName;
    private String user;
    private int version;
    private Date createTime;
    private Date updateTime;

    public static DeploymentResp apply(ProcessDefinition processDefinition, Date deploymentTime) {
        return DeploymentResp.builder()
                .id(processDefinition.getId())
                .processId(processDefinition.getId())
                .processKey(processDefinition.getKey())
                .name(processDefinition.getName())
                .version(processDefinition.getVersion())
                .resourceName(processDefinition.getResourceName())
                .createTime(deploymentTime)
                .build();
    }
}
