package com.sepp.flowplus.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class CommonReq {

    @NotEmpty(message = "用户id不能为空")
    private String userId;
    @NotEmpty(message = "用户名称不能为空")
    private String userName;

    private Integer pageNum = 1;
    private Integer pageSize = 10;

}
