package com.sepp.flowplus.configuration.datasource;

import com.sepp.flowplus.auth.threadlocal.DataSourceContextHolder;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import static com.sepp.flowplus.configuration.datasource.DataSourceConfig.SEPP_DATASOURCE_SEPP_KEY;

@Aspect
@Component
public class DatasourceChangeAspect {

    @Around("execution(public * com.sepp.flowplus.mapper.*.*(..))")
    public Object printExecuteIntervalLog(ProceedingJoinPoint point) throws Throwable {
        Object proceed;
        DataSourceContextHolder.switchDataSource(SEPP_DATASOURCE_SEPP_KEY);
        proceed = point.proceed();
        DataSourceContextHolder.clear();
        return proceed;
    }

}
