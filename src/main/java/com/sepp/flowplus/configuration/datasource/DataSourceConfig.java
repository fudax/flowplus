package com.sepp.flowplus.configuration.datasource;

import com.google.common.collect.Maps;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.Map;

import static com.sepp.flowplus.auth.AESEncryptor.convertProperty;

@Configuration
public class DataSourceConfig {

    public static final String SEPP_DATASOURCE_FLOWABLE_KEY = "flowable";
    public static final String SEPP_DATASOURCE_SEPP_KEY = "sepp";

    @Value("${datasource.flowplus.url}")
    private String flowplusUrl;
    @Value("${datasource.flowplus.username}")
    private String flowplusUsername;
    @Value("${datasource.flowplus.password}")
    private String flowplusPassword;

    @Value("${datasource.sepp.url}")
    private String seppUrl;
    @Value("${datasource.sepp.username}")
    private String seppUsername;
    @Value("${datasource.sepp.password}")
    private String seppPassword;

    public DataSource flowplusDataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(flowplusUrl);
        dataSource.setUsername(flowplusUsername);
        dataSource.setPassword(flowplusPassword);
        return dataSource;
    }

    public DataSource seppDataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(seppUrl);
        dataSource.setUsername(convertProperty(seppUsername));
        dataSource.setPassword(convertProperty(seppPassword));
        return dataSource;
    }

    @Bean(name = "routingDataSource")
    public DynamicDataSource routingDataSource() {
        Map<Object, Object> dataSourceMap = Maps.newHashMap();
        dataSourceMap.put(SEPP_DATASOURCE_SEPP_KEY, seppDataSource());
        dataSourceMap.put(SEPP_DATASOURCE_FLOWABLE_KEY, flowplusDataSource());
        DynamicDataSource routingDataSource = new DynamicDataSource();
        routingDataSource.setTargetDataSources(dataSourceMap);
        routingDataSource.setDefaultTargetDataSource(flowplusDataSource());
        return routingDataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(seppDataSource());
    }
}