package com.sepp.flowplus.configuration.datasource;

import com.sepp.flowplus.auth.threadlocal.DataSourceContextHolder;
import com.sun.xml.internal.ws.encoding.DataHandlerDataSource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import static com.sepp.flowplus.configuration.datasource.DataSourceConfig.SEPP_DATASOURCE_FLOWABLE_KEY;
import static com.sepp.flowplus.configuration.datasource.DataSourceConfig.SEPP_DATASOURCE_SEPP_KEY;


public class DynamicDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        System.out.println(DataSourceContextHolder.getDataSource()+"--------");
        if (StringUtils.isNotBlank(DataSourceContextHolder.getDataSource()) &&
           SEPP_DATASOURCE_SEPP_KEY.equalsIgnoreCase(DataSourceContextHolder.getDataSource())) {
            return DataSourceContextHolder.getDataSource();
        }
        return SEPP_DATASOURCE_FLOWABLE_KEY;
    }



}
