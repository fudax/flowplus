package com.sepp.flowplus.configuration.health.service;

import com.sepp.flowplus.mapper.HomeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HomeService {

    @Autowired
    private HomeMapper homeMapper;

    public void select() {
        homeMapper.selectOne();
    }
}
