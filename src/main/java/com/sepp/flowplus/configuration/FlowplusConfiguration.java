package com.sepp.flowplus.configuration;

import com.google.common.collect.Maps;
import com.sepp.flowplus.flowable.FlowableHelper;
import com.sepp.flowplus.flowable.event.GlobalTaskEndEventListener;
import com.sepp.flowplus.flowable.event.GlobalProcessInstanceEndEventListener;
import com.sepp.flowplus.flowable.notifier.MailProcessEndNotifier;
import com.sepp.flowplus.flowable.query.MyHandledTaskQuery;
import com.sepp.flowplus.flowable.query.MyHandledTaskQueryImpl;
import com.sepp.flowplus.flowable.query.MyStartedTaskQuery;
import com.sepp.flowplus.mapper.UserMapper;
import org.flowable.common.engine.api.delegate.event.FlowableEventListener;
import org.flowable.engine.HistoryService;
import org.flowable.spring.SpringProcessEngineConfiguration;
import org.flowable.spring.boot.EngineConfigurationConfigurer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.flowable.common.engine.api.delegate.event.FlowableEngineEventType.PROCESS_COMPLETED;
import static org.flowable.common.engine.api.delegate.event.FlowableEngineEventType.TASK_COMPLETED;

@Configuration
public class FlowplusConfiguration {


    @Bean
    @ConditionalOnMissingBean
    public MyHandledTaskQuery taskQuery(HistoryService historyService) {
        return new MyHandledTaskQueryImpl(historyService);
    }

    @Bean
    @ConditionalOnMissingBean
    public MyStartedTaskQuery myProcessTaskQuery(HistoryService historyService) {
        return new MyStartedTaskQuery(historyService);
    }

    /**
     * 添加自定义监听器
     *
     * @param globalTaskEndEventListener
     * @return
     */
    @Bean
    public EngineConfigurationConfigurer<SpringProcessEngineConfiguration> globalListenerConfigurer(GlobalTaskEndEventListener globalTaskEndEventListener,
                                                                                                    GlobalProcessInstanceEndEventListener processInstanceEndEventListener) {
        Map<String, List<FlowableEventListener>> listenerMap = Maps.newHashMap();
        listenerMap.put(TASK_COMPLETED.name(), singletonList(globalTaskEndEventListener));
        listenerMap.put(PROCESS_COMPLETED.name(), singletonList(processInstanceEndEventListener));
        return engineConfiguration -> engineConfiguration.setTypedEventListeners(listenerMap);
    }

    @Bean
    @ConditionalOnMissingBean
    public FlowableHelper flowableHelper() {
        return new FlowableHelper();
    }


    @Bean
    @ConfigurationProperties("flowable.notify.mail")
    public MailProcessEndNotifier mailProcessEndNotifier(JavaMailSender mailSender,
                                                         UserMapper userMapper,
                                                         JdbcTemplate jdbcTemplate) {
        return new MailProcessEndNotifier(mailSender, userMapper,jdbcTemplate);
    }


}
