package com.sepp.flowplus.auth.filter;

import com.sepp.flowplus.auth.threadlocal.ParameterThreadLocal;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.util.UUID;

@Component
@WebFilter(urlPatterns = "/*", filterName = "parameterFilter")
public class ParameterFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String userId = request.getParameter("userId");
        String productId = request.getParameter("productId");
        if (StringUtils.isNotBlank(userId)) {
            ParameterThreadLocal.setUserId(userId);
        }
        if (StringUtils.isNotBlank(productId)) {
            ParameterThreadLocal.setProductId(Integer.valueOf(productId));
        }
        ParameterThreadLocal.setHttpId(UUID.randomUUID().toString());
        chain.doFilter(request, response);
    }

}
