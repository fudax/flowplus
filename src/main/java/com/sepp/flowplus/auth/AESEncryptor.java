package com.sepp.flowplus.auth;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static com.google.common.base.Strings.nullToEmpty;

public class AESEncryptor {

	private static final String ERROR_EN_MSG = "用户名密码解密失败";
	private static final String ERROR_DE_MSG = "用户名密码解密失败";
	private static final String CIPHER_INSTANCE = "AES/CBC/PKCS5Padding";
	private static final String SERIAL_KEY = "0102030405060708";
	private static final String ENCRYPT_STYLE = "AES";
    private static final String AES_KEY = "0807060504030201";

	public static String decrypt(String toDecrypted, String sKey) {
		try {
			if (sKey == null || sKey.length() != 16) {
				return null;
			}
			byte[] raw = sKey.getBytes("ASCII");
			SecretKeySpec skeySpec = new SecretKeySpec(raw, ENCRYPT_STYLE);
			Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE);
			IvParameterSpec iv = new IvParameterSpec(SERIAL_KEY.getBytes());
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
			byte[] encrypted1 = org.apache.commons.codec.binary.Base64.decodeBase64(toDecrypted);
			try {
				return new String(cipher.doFinal(encrypted1));
			} catch (Exception e) {
				throw e;
			}
		} catch (Exception ex) {
			throw new RuntimeException(ERROR_DE_MSG, ex);
		}
	}

    public static String convertProperty(String value) {
        return AESEncryptor.decrypt(nullToEmpty(value), AES_KEY);
    }
}