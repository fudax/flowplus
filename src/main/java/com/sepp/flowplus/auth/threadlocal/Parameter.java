package com.sepp.flowplus.auth.threadlocal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Parameter {
    private String userId;
    private String userName;
    private String name;
    private Integer productId;
    private String email;
    private String httpId;

}
