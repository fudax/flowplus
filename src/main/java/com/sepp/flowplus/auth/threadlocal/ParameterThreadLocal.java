package com.sepp.flowplus.auth.threadlocal;


public final class ParameterThreadLocal {
    private ParameterThreadLocal() {}

    public static final ThreadLocal<Parameter> PARAMETER_THREAD_LOCAL = new ThreadLocal<>();

    public static void setUserId(String uid) {
        Parameter parameter = getParameter();
        parameter.setUserId(uid);
    }

    public static String getUserId() {
        Parameter parameter = PARAMETER_THREAD_LOCAL.get();
        if (parameter != null) {
            return parameter.getUserId();
        }
        return null;
    }

    public static void setProductId(Integer productId) {
        Parameter parameter = getParameter();
        parameter.setProductId(productId);
    }


    public static void setHttpId(String httpId) {
        Parameter parameter = getParameter();
        parameter.setHttpId(httpId);
    }

    private static Parameter getParameter() {
        Parameter parameter = PARAMETER_THREAD_LOCAL.get();
        if (parameter == null) {
            parameter = Parameter.builder().build();
            PARAMETER_THREAD_LOCAL.set(parameter);
        }
        return parameter;
    }

    public static void clear() {
        PARAMETER_THREAD_LOCAL.remove();
    }

}
