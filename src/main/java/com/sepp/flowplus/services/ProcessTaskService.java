package com.sepp.flowplus.services;

import com.sepp.flowplus.flowable.query.MyHandledTaskQuery;
import com.sepp.flowplus.flowable.query.MyStartedTaskQuery;
import com.sepp.flowplus.model.TaskQueryCondition;
import com.sepp.flowplus.model.TaskResp;
import com.sepp.flowplus.model.TaskRespDetail;
import com.sepp.flowplus.model.TaskVariable;
import org.flowable.engine.TaskService;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.sepp.flowplus.model.TaskVariable.tagsToMap;

@Service
public class ProcessTaskService {

    @Autowired
    private TaskService taskService;
    @Autowired
    private MyStartedTaskQuery myStartedTaskQuery;
    @Autowired
    private MyHandledTaskQuery myHandledTaskQuery;

    /**
     * 待处理任务
     *
     * @param userId
     * @param pageNum
     * @param pageSize
     * @return
     */
    public List<TaskResp> listTasks(String userId, Integer pageNum, Integer pageSize) {
        List<Task> tasks = taskService.createTaskQuery().taskAssignee(userId).listPage((pageNum - 1) * pageSize, pageSize);
        return tasks.stream().map(TaskResp::apply).collect(Collectors.toList());
    }

    /**
     * 我发起的任务查询
     *
     * @param userId
     * @param pageNum
     * @param pageSize
     * @return
     */
    public TaskRespDetail myProcessTasks(String userId, Integer pageNum, Integer pageSize) {
        TaskQueryCondition condition = new TaskQueryCondition();
        condition.setPageNum(pageNum);
        condition.setPageSize(pageSize);
        condition.setUserId(userId);
        condition.setEnd(Date.from(Instant.now()));
        condition.setStart(Date.from(Instant.now().minus(10, ChronoUnit.DAYS)));
        return myStartedTaskQuery.query(condition);
    }

    /**
     * 审批任务
     */
    public void complete(TaskVariable taskVariable) {
        //fixme 完成接口只对应对应的任务负责人才有权限
        Map<String, Object> variables = tagsToMap(taskVariable.getTags());
        variables.put("approve", taskVariable.getAuditStatus().name());
        taskService.complete(taskVariable.getTaskId(), variables);
    }

    /**
     * 委托任务（该任务委托给其他人）
     */
    public void delegateTask(TaskVariable taskVariable) {
        taskService.delegateTask(taskVariable.getTaskId(), taskVariable.getDelegateUser());
    }

    public List<TaskResp> unprocessedTasks(String userId) {
        return myHandledTaskQuery.unprocessedTasks(userId);
    }

    public List<TaskResp> expiredTasks(String userId) {
        return null;
    }

    public List<TaskResp> completedTasks(String userId) {
        return myHandledTaskQuery.completedTasks(userId);
    }
}
