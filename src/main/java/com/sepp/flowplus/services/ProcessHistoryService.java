package com.sepp.flowplus.services;

import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.history.HistoricProcessInstanceQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 流程查询
 */
@Service
public class ProcessHistoryService {

    @Autowired
    private HistoryService historyService;

    public void taskHistories() {
        HistoricProcessInstanceQuery historicProcessInstanceQuery = historyService.createHistoricProcessInstanceQuery();
        System.out.println(historicProcessInstanceQuery);
        List<HistoricProcessInstance> lisi = historyService.createHistoricProcessInstanceQuery().startedBy("lisi")
                .list();
    }


}
