package com.sepp.flowplus.services;

import com.sepp.flowplus.auth.threadlocal.ParameterThreadLocal;
import com.sepp.flowplus.model.ProcessInstance;
import com.sepp.flowplus.model.Tag;
import com.sepp.flowplus.model.TaskVariable;
import org.flowable.engine.IdentityService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentQuery;
import org.flowable.engine.runtime.ProcessInstanceQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toMap;

@Service
public class ProcessInstanceService {

    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private IdentityService identityService;
    @Autowired
    private RepositoryService repositoryService;

    public void start(TaskVariable taskVariable) {
        Map<String, Object> variables = taskVariable.getTags().stream()
                .filter(tag -> nonNull(tag.getTagKey()) && nonNull(tag.getTagValue()))
                .collect(toMap(Tag::getTagKey, Tag::getTagValue));
        try {
            //设置发起人userId
            identityService.setAuthenticatedUserId(ParameterThreadLocal.getUserId());
            runtimeService.startProcessInstanceByKey(taskVariable.getProcessDefinitionKey(), variables);
        } finally {
            identityService.setAuthenticatedUserId(null);
        }
    }


}