package com.sepp.flowplus.services;

import com.sepp.flowplus.model.DeploymentResp;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentQuery;
import org.flowable.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProcessDeploymentService {


    @Autowired
    private RepositoryService repositoryService;

    public String deploy(MultipartFile processFile) throws IOException {
        return repositoryService.createDeployment()
                .addInputStream(processFile.getOriginalFilename(), processFile.getInputStream())
                .name("my")
                .deploy()
                .getId();
    }


    /**
     * 获取上传部署的流程列表
     * 对于相同的key的流程被认定为同一个流程，添加或修改都会覆盖上一次的流程，版本加1
     *
     * @return
     */
    public List<DeploymentResp> listProcessInstancesPaging() {
        //只能获取部署文件后缀为(.bpmn20.xml)的流程
        List<ProcessDefinition> processDefinitions = repositoryService.createProcessDefinitionQuery()
                .orderByProcessDefinitionKey()
                .desc()
                .latestVersion()
                .listPage(0, 10);
        DeploymentQuery deploymentQuery = repositoryService.createDeploymentQuery();
        return processDefinitions.stream().map(processDefinition -> {
            Deployment deployment = deploymentQuery.deploymentId(processDefinition.getDeploymentId())
                    .singleResult();
            return DeploymentResp.apply(processDefinition,deployment.getDeploymentTime());
        }).collect(Collectors.toList());
    }
}