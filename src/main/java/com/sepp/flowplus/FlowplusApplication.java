package com.sepp.flowplus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.sepp.flowplus.mapper.*")
public class FlowplusApplication {

    public static void main(String[] args) {
        SpringApplication.run(FlowplusApplication.class, args);
    }

}
