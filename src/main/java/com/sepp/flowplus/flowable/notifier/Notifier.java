package com.sepp.flowplus.flowable.notifier;

public interface Notifier<T> {

    void notify(T t);

}
