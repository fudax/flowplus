package com.sepp.flowplus.flowable.notifier;

public abstract class AbstractEventNotifier<T> implements Notifier<T> {

    private boolean enabled = true;

    @Override
    public void notify(T t) {
        if (!enabled) return;
        if (shouldNotify(t)) {
            doNotify(t);
        }
    }


    protected boolean shouldNotify(T t) {
        return enabled;
    }

    protected abstract void doNotify(T t);


    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
