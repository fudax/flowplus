package com.sepp.flowplus.flowable.notifier;

import com.sepp.flowplus.mapper.UserMapper;
import com.sepp.flowplus.model.User;
import org.flowable.engine.impl.persistence.entity.ExecutionEntityImpl;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;

import static com.sepp.flowplus.model.constants.AuditStatus.processInstanceResult;
import static com.sepp.flowplus.model.constants.CommonSql.SELECT_USER_BY_ID;

/**
 * 流程结束时
 */
public abstract class AbstractFlowEndNotifier<T> extends AbstractEventNotifier<T> {

    private static final String SUBJECT = "您的%s%s";
    private static final String CONTENT = "您的%s%s，请登录能效平台查看详情";

    private String[] to = {"root@localhost"};

    private String[] cc = {};

    private String from = "lchen <noreply@localhost>";

    protected final JavaMailSender mailSender;
    protected final UserMapper userMapper;
    protected final JdbcTemplate jdbcTemplate;

    public AbstractFlowEndNotifier(JavaMailSender mailSender,
                                   UserMapper userMapper,
                                   JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.mailSender = mailSender;
        this.userMapper = userMapper;
    }

    protected MimeMessage buildMimeMessage(ExecutionEntityImpl entity) throws MessagingException {
        //任务结果
        String approve = (String) entity.getVariable("approve");
        String result = processInstanceResult(approve);
        //任务名称
        String name = entity.getName();
        String sql = SELECT_USER_BY_ID;
        RowMapper<User> rowMapper = new BeanPropertyRowMapper<>(User.class);
        User user = jdbcTemplate.queryForObject(sql, rowMapper, entity.getStartUserId());
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, StandardCharsets.UTF_8.name());
        message.setSubject(String.format(SUBJECT, name, result));
        message.setText(String.format(CONTENT,name,result));
        message.setTo(user.getUserEmail());
        message.setFrom(this.getFrom());
        return mimeMessage;
    }

    public String[] getTo() {
        return to;
    }

    public void setTo(String[] to) {
        this.to = to;
    }

    public String[] getCc() {
        return cc;
    }

    public void setCc(String[] cc) {
        this.cc = cc;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }
}
