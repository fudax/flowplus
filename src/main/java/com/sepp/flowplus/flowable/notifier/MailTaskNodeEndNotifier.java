package com.sepp.flowplus.flowable.notifier;

import com.sepp.flowplus.mapper.UserMapper;
import org.flowable.task.service.impl.persistence.entity.TaskEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mail.javamail.JavaMailSender;

/**
 * 当前任务节点结束后通知下一个节点审批人
 */
public class MailTaskNodeEndNotifier extends AbstractFlowEndNotifier<TaskEntity> {

    public MailTaskNodeEndNotifier(JavaMailSender mailSender, UserMapper userMapper,
                                   JdbcTemplate jdbcTemplate) {
        super(mailSender, userMapper,jdbcTemplate);
    }

    @Override
    protected void doNotify(TaskEntity event) {

    }

}
