package com.sepp.flowplus.flowable.notifier;

import com.sepp.flowplus.configuration.TaskSchedulerConfig;
import com.sepp.flowplus.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.impl.persistence.entity.ExecutionEntityImpl;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * 当整个任务流程结束的时候通知到任务发起人
 */
@Slf4j
public class MailProcessEndNotifier extends AbstractFlowEndNotifier<ExecutionEntityImpl> {

    public MailProcessEndNotifier(JavaMailSender mailSender,
                                  UserMapper userMapper,
                                  JdbcTemplate jdbcTemplate) {
        super(mailSender, userMapper, jdbcTemplate);
    }

    @Override
    protected void doNotify(ExecutionEntityImpl entity) {

        try {
            MimeMessage mimeMessage = buildMimeMessage(entity);
            TaskSchedulerConfig.getExecutor().submit(() -> mailSender.send(mimeMessage));
        } catch (MessagingException e) {
            log.error("发送邮件失败:{}", e);
        }
    }


}
