package com.sepp.flowplus.flowable.event;

import com.sepp.flowplus.flowable.FlowableHelper;
import lombok.extern.slf4j.Slf4j;
import org.flowable.bpmn.model.FlowNode;
import org.flowable.common.engine.api.delegate.event.AbstractFlowableEventListener;
import org.flowable.common.engine.api.delegate.event.FlowableEvent;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.delegate.event.impl.FlowableEntityEventImpl;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.spring.ProcessEngineFactoryBean;
import org.flowable.task.api.Task;
import org.flowable.task.service.impl.persistence.entity.TaskEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 全局事件监听器
 */
@Slf4j
@Component
public class GlobalTaskEndEventListener extends AbstractFlowableEventListener {

    @Lazy
    @Autowired
    private ProcessEngineFactoryBean factoryBean;

    @Autowired
    private FlowableHelper flowableHelper;

    @Override
    public void onEvent(FlowableEvent event) {
        TaskEntity taskEntity = (TaskEntity) ((FlowableEntityEventImpl) event).getEntity();
        //通知
        notify(taskEntity);

    }

    private void notify(TaskEntity taskEntity) {
        ProcessEngine processEngine = null;
        try {
            processEngine = factoryBean.getObject();
        } catch (Exception e) {
            log.error("流程引擎获取失败{}", e);
        }
        FlowNode currentFlowNode = flowableHelper.currentFlowNode(processEngine, taskEntity.getId());
        assert processEngine != null;
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().taskId("").list();
        HistoricProcessInstance historicProcessInstance = processEngine.getHistoryService().createHistoricProcessInstanceQuery()
                .processInstanceId(taskEntity.getProcessInstanceId())
                .singleResult();
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }
}
