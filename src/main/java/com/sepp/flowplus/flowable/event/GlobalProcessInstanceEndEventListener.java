package com.sepp.flowplus.flowable.event;


import com.sepp.flowplus.configuration.CommonFactory;
import com.sepp.flowplus.flowable.notifier.Notifier;
import org.flowable.common.engine.api.delegate.event.AbstractFlowableEventListener;
import org.flowable.common.engine.api.delegate.event.FlowableEvent;
import org.flowable.engine.delegate.event.impl.FlowableEntityEventImpl;
import org.flowable.engine.impl.persistence.entity.ExecutionEntityImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 流程结束监听
 */
@Component
public class GlobalProcessInstanceEndEventListener extends AbstractFlowableEventListener {


    @Autowired
    private CommonFactory commonFactory;

    @Override
    public void onEvent(FlowableEvent event) {
        ExecutionEntityImpl entity = (ExecutionEntityImpl) ((FlowableEntityEventImpl) event).getEntity();
        notify(entity);
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }

    private void notify(ExecutionEntityImpl entity) {
        Notifier notifier = commonFactory.getBean("mailProcessEndNotifier");
        notifier.notify(entity);
    }
}
