package com.sepp.flowplus.flowable;

import org.flowable.bpmn.model.BpmnModel;
import org.flowable.bpmn.model.FlowNode;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.impl.persistence.entity.ExecutionEntity;
import org.flowable.task.api.Task;


public class FlowableHelper {




    /**
     * 当前流程节点
     */
    public FlowNode currentFlowNode(ProcessEngine processEngine, String taskId) {
        Task task = processEngine.getTaskService().createTaskQuery().taskId(taskId).singleResult();
        ExecutionEntity executionEntity = (ExecutionEntity) processEngine.getRuntimeService().createExecutionQuery()
                .executionId(task.getExecutionId()).singleResult();
        // 当前审批节点
        String currentActivityId = executionEntity.getActivityId();
        BpmnModel bpmnModel = processEngine.getRepositoryService().getBpmnModel(task.getProcessDefinitionId());
        return (FlowNode) bpmnModel.getFlowElement(currentActivityId);
    }

    /**
     * 下一个流程的节点
     */
    public void nextFlowNode() {
        System.out.println("");
    }


}
