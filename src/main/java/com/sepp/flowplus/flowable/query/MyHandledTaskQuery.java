package com.sepp.flowplus.flowable.query;

import com.sepp.flowplus.model.TaskResp;

import java.util.List;

public interface MyHandledTaskQuery {


    /**
     * 已完成的任务
     */
    List<TaskResp> completedTasks(String userId);

    /**
     * 过期的任务
     */
    List<TaskResp> expiredTasks(String userId);

    /**
     * 待处理的任务
     */
    List<TaskResp> unprocessedTasks(String userId);

}
