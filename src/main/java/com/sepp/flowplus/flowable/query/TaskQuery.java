package com.sepp.flowplus.flowable.query;

import com.sepp.flowplus.model.TaskQueryCondition;
import com.sepp.flowplus.model.TaskRespDetail;

public interface TaskQuery<T> {

    T query(TaskQueryCondition condition);

}
