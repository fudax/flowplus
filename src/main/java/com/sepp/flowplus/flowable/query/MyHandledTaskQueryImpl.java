package com.sepp.flowplus.flowable.query;

import com.sepp.flowplus.model.TaskResp;
import org.flowable.engine.HistoryService;
import org.flowable.task.api.history.HistoricTaskInstance;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 我需要处理的任务
 */
public class MyHandledTaskQueryImpl implements MyHandledTaskQuery {

    private final HistoryService historyService;

    public MyHandledTaskQueryImpl(HistoryService historyService) {
        this.historyService = historyService;
    }

    @Override
    public List<TaskResp> completedTasks(String userId) {
        List<HistoricTaskInstance> historicTaskInstances = historyService.createHistoricTaskInstanceQuery()
                .taskAssignee(userId)
                .finished()
                .list();
        return historicTaskInstances.stream().map(TaskResp::apply).collect(Collectors.toList());
    }

    @Override
    public List<TaskResp> expiredTasks(String userId) {
//        historyService.createHistoricTaskInstanceQuery()
//                .taskAssignee(userId)
//                .finished()
//                .dueda


        return null;
    }

    @Override
    public List<TaskResp> unprocessedTasks(String userId) {
        List<HistoricTaskInstance> historicTaskInstances = historyService.createHistoricTaskInstanceQuery()
                .taskAssignee(userId)
                .unfinished()
                .list();
        return historicTaskInstances.stream().map(TaskResp::apply).collect(Collectors.toList());
    }
}
