package com.sepp.flowplus.flowable.query;

import com.sepp.flowplus.model.TaskQueryCondition;
import com.sepp.flowplus.model.TaskResp;
import com.sepp.flowplus.model.TaskRespDetail;
import com.sepp.flowplus.model.constants.AuditStatus;
import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricProcessInstance;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 我发起的任务查询接口
 */
public class MyStartedTaskQuery implements TaskQuery<TaskRespDetail> {

    private final HistoryService historyService;

    public MyStartedTaskQuery(HistoryService historyService) {
        this.historyService = historyService;
    }

    @Override
    public TaskRespDetail query(TaskQueryCondition condition) {
        List<HistoricProcessInstance> historicProcessInstances = queryHistoricProcessInstances(condition);
        return TaskRespDetail.builder()
                .pendingTasks(pendingTasks(condition))
                .approvedTasks(approvedTasks(historicProcessInstances))
                .rejectedTasks(rejectedTasks(historicProcessInstances))
                .expiredTasks(expiredTasks(historicProcessInstances))
                .returnTasks(returnTasks(historicProcessInstances))
                .build();
    }


    /**
     * 正在进行中的任务
     *
     * @return
     */
    public List<TaskResp> pendingTasks(TaskQueryCondition condition) {
        List<HistoricProcessInstance> historicProcessInstances = historyService.createHistoricProcessInstanceQuery()
                .startedBy(condition.getUserId())
                .unfinished()
                .startedAfter(condition.getStart())
                .startedBefore(condition.getEnd())
                .listPage(condition.firstNum(), condition.getPageSize());
        return historicProcessInstances.stream().map(TaskResp::apply).collect(Collectors.toList());
    }

    /**
     * 通过的任务
     *
     * @return
     */
    public List<TaskResp> approvedTasks(TaskQueryCondition condition) {
        return queryTasksByStatus(condition, AuditStatus.APPROVE);
    }

    /**
     * 过期的尚未被处理完成的任务
     *
     * @return
     */
    public List<TaskResp> expiredTasks(TaskQueryCondition condition) {
        return queryTasksByStatus(condition, AuditStatus.EXPIRED);

    }

    /**
     * 被拒绝的任务
     *
     * @return
     */
    public List<TaskResp> rejectedTasks(TaskQueryCondition condition) {
        return queryTasksByStatus(condition, AuditStatus.REJECT);
    }

    /**
     * 被退回的任务(踢退)
     *
     * @param condition
     * @return
     */
    public List<TaskResp> returnTasks(TaskQueryCondition condition) {
        return queryTasksByStatus(condition, AuditStatus.RETURN);
    }

    private List<TaskResp> approvedTasks(List<HistoricProcessInstance> historicProcessInstances) {
        return queryTasksByStatus(historicProcessInstances, AuditStatus.APPROVE);
    }

    private List<TaskResp> expiredTasks(List<HistoricProcessInstance> historicProcessInstances) {
        return queryTasksByStatus(historicProcessInstances, AuditStatus.EXPIRED);

    }

    private List<TaskResp> rejectedTasks(List<HistoricProcessInstance> historicProcessInstances) {
        return queryTasksByStatus(historicProcessInstances, AuditStatus.REJECT);
    }


    private List<TaskResp> returnTasks(List<HistoricProcessInstance> historicProcessInstances) {
        return queryTasksByStatus(historicProcessInstances, AuditStatus.RETURN);
    }


    private List<TaskResp> queryTasksByStatus(TaskQueryCondition condition, AuditStatus status) {
        return tasksFilter(status, queryHistoricProcessInstances(condition));

    }


    private List<TaskResp> queryTasksByStatus(List<HistoricProcessInstance> historicProcessInstances, AuditStatus status) {
        return tasksFilter(status, historicProcessInstances);
    }

    private List<TaskResp> tasksFilter(AuditStatus status, List<HistoricProcessInstance> historicProcessInstances) {
        //内存筛选出对应流程节点状态的任务
        List<HistoricProcessInstance> historicProcessInstanceList = historicProcessInstances.stream()
                .filter(historicProcessInstance -> status.name().equalsIgnoreCase(historicProcessInstance.getEndActivityId()))
                .collect(Collectors.toList());

        return historicProcessInstanceList.stream().map(TaskResp::apply).collect(Collectors.toList());

//        List<String> instanceIds = historicProcessInstanceList.stream().map(HistoricProcessInstance::getId).collect(Collectors.toList());
//
//        if (CollectionUtils.isEmpty(instanceIds)) {
//            return Lists.newArrayList();
//        }
//
//        List<HistoricTaskInstance> historicTaskInstances = historyService.createHistoricTaskInstanceQuery()
//                .processInstanceIdIn(instanceIds)
//                .list();
//
//        return historicTaskInstances.stream().map(TaskResp::apply).collect(Collectors.toList());
    }

    private List<HistoricProcessInstance> queryHistoricProcessInstances(TaskQueryCondition condition) {
        return historyService.createHistoricProcessInstanceQuery()
                .startedBy(condition.getUserId())
                .finished()
                .startedAfter(condition.getStart())
                .startedBefore(condition.getEnd())
                .list();
    }

}
