package com.sepp.flowplus.controller;

import com.sepp.flowplus.model.DeploymentResp;
import com.sepp.flowplus.services.ProcessDeploymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
public class ProcessDeploymentController {

    @Autowired
    private ProcessDeploymentService processDeploymentService;

    @PostMapping(value = "/process/deployment")
    public String deploy(@RequestParam("uploadFile") MultipartFile processFile) throws IOException {
        return processDeploymentService.deploy(processFile);
    }

    @GetMapping(value = "/process/deployment")
    public List<DeploymentResp> listProcessInstancesPaging() {
        return processDeploymentService.listProcessInstancesPaging();
    }

}
