package com.sepp.flowplus.controller;

import com.sepp.flowplus.model.TaskVariable;
import com.sepp.flowplus.services.ProcessInstanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProcessInstanceController {
    @Autowired
    private ProcessInstanceService processInstanceService;

    @PostMapping(value = "/process-instance/start")
    public void startProcessInstance(@RequestBody TaskVariable taskVariable) {
        processInstanceService.start(taskVariable);
    }


}
