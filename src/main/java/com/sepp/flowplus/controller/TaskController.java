package com.sepp.flowplus.controller;

import com.sepp.flowplus.auth.threadlocal.ParameterThreadLocal;
import com.sepp.flowplus.model.TaskResp;
import com.sepp.flowplus.model.TaskRespDetail;
import com.sepp.flowplus.model.TaskVariable;
import com.sepp.flowplus.services.ProcessTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TaskController {

    @Autowired
    private ProcessTaskService processTaskService;

    @GetMapping(value = "/tasks")
    public List<TaskResp> listTaskPaging(@RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                                         @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize) {
        return processTaskService.listTasks(ParameterThreadLocal.getUserId(), pageNum, pageSize);
    }

    @GetMapping(value = "/tasks/myself", name = "我发起的任务")
    public TaskRespDetail listTasks(@RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                                    @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize) {
        return processTaskService.myProcessTasks(ParameterThreadLocal.getUserId(), pageNum, pageSize);
    }

    @PostMapping(value = "/tasks::complete", name = "任务完成接口：通过、驳回、踢退")
    public void complete(@RequestBody TaskVariable taskVariable) {
        processTaskService.complete(taskVariable);
    }

    @PostMapping(value = "/tasks::delegate", name = "任务委托")
    public void delegateTask(@RequestBody TaskVariable taskVariable) {
        processTaskService.delegateTask(taskVariable);
    }

    @GetMapping(value = "/tasks/unprocessed", name = "待处理的任务")
    public List<TaskResp> unprocessedTasks() {
        return processTaskService.unprocessedTasks(ParameterThreadLocal.getUserId());
    }

    @GetMapping(value = "/tasks/completed", name = "已经处理的任务")
    public List<TaskResp> completedTasks() {
        return processTaskService.completedTasks(ParameterThreadLocal.getUserId());
    }

    @GetMapping(value = "/tasks/expired", name = "已过期任务")
    public List<TaskResp> expiredTasks() {
        return processTaskService.expiredTasks(ParameterThreadLocal.getUserId());
    }

}
