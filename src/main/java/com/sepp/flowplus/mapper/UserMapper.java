package com.sepp.flowplus.mapper;

import com.sepp.flowplus.model.User;

public interface UserMapper {

    Integer countUsers(String username);

    User findUserByUserId(String userId);
}
