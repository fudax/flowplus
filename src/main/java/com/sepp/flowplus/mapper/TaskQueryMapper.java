package com.sepp.flowplus.mapper;

import com.sepp.flowplus.model.history.ActHiNodeStatus;

import java.util.List;

public interface TaskQueryMapper {

    List<ActHiNodeStatus> queryActHiNodeStatus();

}
