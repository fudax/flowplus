import MyStartedTask from '../components/MyStartedTask';

const routers = [{
    path: '/',
    component: MyStartedTask
}, {
    path: '/myTask',
    component: MyStartedTask
}];

export default routers;