import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import routes from './router/router';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from 'axios';


Vue.config.productionTip = false;
axios.defaults.baseURL = 'http://localhost:8082/flowplus';

Vue.use(ElementUI);
Vue.use(VueRouter);


const router = new VueRouter({
    routes
});


new Vue({
    router,
    render: h => h(App),
}).$mount('#app');
